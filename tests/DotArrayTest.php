<?php

namespace Arcesilas\DotArray\Tests;

use PHPUnit\Framework\TestCase;
use Arcesilas\DotArray\DotArray;
use ArrayObject;

class DotArrayTest extends TestCase
{
    protected $dotArray;

    public function setUp()
    {
        $this->input = [
            'a' => 'b',
            'c' => [
                'd',
                'e'
            ],
            'f' => [
                'g' => 'h',
                'i' => 'j'
            ],
            'k' => 42,
            'l' => null,
            'm' => ''
        ];

        $this->dotArray = new DotArray($this->input);
    }

    public function hasProvider()
    {
        return [
            ['a', true],
            ['b', false],
            ['f.g', true],
            ['x.y', false]
        ];
    }

    /**
     * @dataProvider hasProvider
     */
    public function testHas($key, $expected)
    {
        $this->assertSame($expected, $this->dotArray->has($key));
    }

    public function getProvider()
    {
        return [
            ['a', 'b'],
            ['c', ['d', 'e']],
            ['f.g', 'h'],
            ['k', 42],
            ['l', null],
            ['m', ''],
            ['x', 'zzzzz'],
            ['x.y.z', 'zzzzz']
        ];
    }

    /**
     * @dataProvider getProvider
     */
    public function testGet($key, $expected)
    {
        $this->assertEquals($expected, $this->dotArray->get($key, 'zzzzz'));
    }

    public function testGetRunsClosureForDefaultValue()
    {
        $this->assertEquals(42, $this->dotArray->get('undefined', function () {
            return 6 * 7;
        }));
    }

    /**
     * @dataProvider getProvider
     */
    public function testHasThenGet($key, $expected)
    {
        $has = ($expected !== 'zzzzz');
        $this->assertSame($has, $this->dotArray->has($key));
        $this->assertEquals($expected, $this->dotArray->get($key, 'zzzzz'));
    }

    public function setProvider()
    {
        return [
            ['n', 'o', 'n', 'o'],
            ['p.q', 'r', 'p', ['q' => 'r']],
            ['a', 'z', 'a', 'z'],
            ['f.g', 'z', 'f', ['g' => 'z', 'i' => 'j']]
        ];
    }

    /**
     * @dataProvider setProvider
     */
    public function testSet($setKey, $setValue, $getKey, $expectedValue)
    {
        $this->dotArray->set($setKey, $setValue);
        $ref = new \ReflectionObject($this->dotArray);
        $refInput = $ref->getProperty('input');
        $refInput->setAccessible(true);
        $actualInput = $refInput->getValue($this->dotArray);

        $this->assertEquals($expectedValue, $actualInput[$getKey]);
    }

    public function testGetArrayCopy()
    {
        $this->assertSame($this->input, $this->dotArray->getArrayCopy());
    }

    public function testThrowsExceptionWhenInvalidInputGiven()
    {
        $this->expectException(\TypeError::class);
        $dotArray = new DotArray('string');
    }

    public function testImportArray()
    {
        $import = [
            'a' => 'xyz',
            'x' => 'y',
            'f' => ['g' => 'fgh'], // f.g => fgh
            'w' => ['v' => ['u' => 't']] // w.v.u => t
        ];

        $this->dotArray->import($import);
        $this->assertEquals('xyz', $this->dotArray->get('a'));
        $this->assertEquals('y', $this->dotArray->get('x'));
        $this->assertEquals('fgh', $this->dotArray->get('f.g'));
        $this->assertEquals('t', $this->dotArray->get('w.v.u'));
        $this->assertEquals(['u' => 't'], $this->dotArray->get('w.v'));
    }

    public function testImportObject()
    {
        $array = [
            'a' => 'xyz',
            'x' => 'y',
            'f' => ['g' => 'fgh'], // f.g => fgh
            'w' => ['v' => ['u' => 't']] // w.v.u => t
        ];

        $ao = new ArrayObject($array);
        $this->dotArray->import($ao);
        $this->assertEquals('xyz', $this->dotArray->get('a'));
        $this->assertEquals('y', $this->dotArray->get('x'));
        $this->assertEquals('fgh', $this->dotArray->get('f.g'));
        $this->assertEquals('t', $this->dotArray->get('w.v.u'));
        $this->assertEquals(['u' => 't'], $this->dotArray->get('w.v'));
    }

    public function testImportDotArray()
    {
        $array = [
            'a' => 'xyz',
            'x' => 'y',
            'f' => ['g' => 'fgh'], // f.g => fgh
            'w' => ['v' => ['u' => 't']] // w.v.u => t
        ];

        $da = new DotArray($array);
        $this->dotArray->import($da);
        $this->assertEquals('xyz', $this->dotArray->get('a'));
        $this->assertEquals('y', $this->dotArray->get('x'));
        $this->assertEquals('fgh', $this->dotArray->get('f.g'));
        $this->assertEquals('t', $this->dotArray->get('w.v.u'));
    }

    public function testDoesNotImportInvalidInput()
    {
        $this->dotArray->import('invalid');
        $this->assertSame($this->input, $this->dotArray->getArrayCopy());
    }

    public function unsetProvider()
    {
        return [
            ['a', true, []],
            ['f.g', true, ['f.i', 'j']],
            ['undefined', false, []]
        ];
    }

    /**
     * @dataProvider unsetProvider
     */
    public function testUnset($key, $has, $check)
    {
        $this->assertSame($has, $this->dotArray->has($key));
        $this->dotArray->unset($key);
        $this->assertFalse($this->dotArray->has($key));

        if (!empty($check)) {
            $this->assertTrue($this->dotArray->has($check[0]));
            $this->assertEquals($check[1], $this->dotArray->get($check[0]));
        }

        if (!$has) {
            $this->assertSame($this->input, $this->dotArray->getArrayCopy());
        }
    }

    public function testArrayAccess()
    {
        $this->assertTrue(isset($this->dotArray['c']));
        $this->assertTrue(isset($this->dotArray['f.g']));

        $this->assertEquals('b', $this->dotArray['a']);
        $this->assertEquals('h', $this->dotArray['f.g']);

        unset($this->dotArray['a']);
        $this->assertFalse(isset($this->dotArray['a']));

        unset($this->dotArray['f.g']);
        $this->assertFalse(isset($this->dotArray['f.g']));

        $this->dotArray['z.y'] = 42;
        $this->assertEquals(42, $this->dotArray['z']['y']);
        $this->assertEquals(42, $this->dotArray->get('z.y'));
    }

    public function testMultipleSet()
    {
        $this->dotArray->set('foo.bar', 42);
        $this->assertEquals(42, $this->dotArray->get('foo.bar'));

        $this->dotArray->set('foo.bar', 1337);
        $this->assertEquals(1337, $this->dotArray->get('foo.bar'));
    }
}
