<?php

namespace Arcesilas\DotArray;

use ArrayAccess;
use ArrayIterator;
use ArrayObject;

class DotArray implements ArrayAccess
{
    /**
     * The input array
     * @var array
     */
    protected $input;

    /**
     * Cached dotted keys and values
     * @var array
     */
    protected $cache = [];

    /**
     * @param  array  $input
     */
    public function __construct(array $input = [])
    {
        $this->input = $input;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        $this->unset($offset);
    }

    /**
     * Returns the default value, executing Closures
     * @param  mixed  $value
     * @return mixed
     */
    protected function default($value)
    {
        return ($value instanceof \Closure) ? $value() : $value;
    }

    /**
     * Returns an item using dot notation
     * @method get
     * @param  string $key
     * @param  mixed $default
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        if (array_key_exists($key, $this->input)) {
            return $this->input[$key];
        } elseif (array_key_exists($key, $this->cache)) {
            return $this->cache[$key];
        }

        // No dot, then not nested keys. We already tried that key and were not lucky.
        if (false === strpos($key, '.')) {
            return $this->default($default);
        }

        try {
            $return = $this->find($this->input, $key);
        } catch (\Exception $e) {
            $return = $this->default($default);
        }
        return $return;
    }

    /**
     * Checks whether the dotted key exists
     * @method has
     * @param  string  $key
     * @return boolean
     */
    public function has(string $key)
    {
        if (array_key_exists($key, $this->input)) {
            return true;
        }

        try {
            $this->find($this->input, $key);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Find a value with its dotted key in an array
     * @param  array $array
     * @param  string $key
     * @return mixed
     * @throws \Exception if the key is not found
     */
    protected function find(array $array, string $key)
    {
        $item = $array;

        foreach (explode('.', $key) as $segment) {
            if (is_array($item) && array_key_exists($segment, $item)) {
                $item = $item[$segment];
            } else {
                throw new \Exception();
            }
        }

        $this->cache[$key] = &$item;

        return $item;
    }

    /**
     * Sets a value using dot notation
     * @param  string $key
     * @param  mixed $value
     */
    public function set(string $key, $value)
    {
        $array = &$this->input;
        $keys = explode('.', $key);

        while (count($keys) > 1) {
            $k = array_shift($keys);
            if (!isset($array[$k]) || ! is_array($array[$k])) {
                $array[$k] = [];
            }
            $array = &$array[$k];
        }

        $array[array_shift($keys)] = $value;
        $this->cache[$key] = &$value;
    }

    /**
     * Unsets a dotted key
     * @param  string $key
     */
    public function unset(string $key)
    {
        if (array_key_exists($key, $this->input)) {
            unset($this->input[$key]);
            unset($this->cache[$key]);
        }

        $item = &$this->input;

        foreach (explode('.', $key) as $segment) {
            if (! isset($item[$segment])) {
                return;
            }
            if (! is_array($item[$segment])) {
                unset($item[$segment]);
                break;
            }
            $item = &$item[$segment];
        }

        unset($this->cache[$key]);
    }

    /**
     * Import an array, ArrayAccess
     * @method import
     * @param  mixed $input
     */
    public function import($input)
    {
        if (false !== ($array = $this->getImportableArray($input))) {
            $this->input = array_replace_recursive($this->input, $array);
            $this->cache = [];
        }
    }

    /**
     * Check whether the input can be imported
     * @param  mixed $input
     * @return bool
     */
    public static function isImportable($input)
    {
        return (
            $input instanceof DotArray ||
            $input instanceof ArrayObject ||
            $input instanceof ArrayIterator
        );
    }

    protected function getImportableArray($input)
    {
        if (is_array($input)) {
            return $input;
        }

        if (static::isImportable($input)) {
            return $input->getArrayCopy();
        }

        return false;
    }

    /**
     * Returns the source array
     * @return array
     */
    public function getArrayCopy()
    {
        return $this->input;
    }
}
