# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2018-02-12
### Changed
- Method export has been replaced by getArrayCopy()
- Instances of ArrayAccess are no longer supported for constructor and import

## [1.0.4] - 2018-01-11
### Fixed cache in set() method

## [1.0.1] - 2018-01-30
### Fixed
- Namespace! ^^

## [1.0.0] - 2018-01-20
- First release
