[![Packagist](https://img.shields.io/packagist/v/arcesilas/dot-array.svg?style=flat-square)]()
[![PHP from Packagist](https://img.shields.io/packagist/php-v/arcesilas/dot-array.svg?style=flat-square)]()
[![license](https://img.shields.io/github/license/Arcesilas/dot-array.svg?style=flat-square)]()
[![Scrutinizer](https://img.shields.io/scrutinizer/g/Arcesilas/dot-array.svg?style=flat-square)]()
[![Scrutinizer Coverage](https://img.shields.io/scrutinizer/coverage/g/Arcesilas/dot-array.svg?style=flat-square)]()
[![Scrutinizer Build](https://img.shields.io/scrutinizer/build/g/Arcesilas/dot-array.svg?style=flat-square)]()

# Arcesilas/DotArray

This package allows access to array items with "dot notation".

Example:

```php
$array = ['a' => ['b' => 'c']];
$dotArray = new DotArray($array);
$dotArray->get('a.b'); // c
```

It is very much inspired by [`Illuminate\Support\Arr`](https://github.com/illuminate/support/blob/master/Arr.php) helper.

## Why yet another dot notation array access?

I needed this tool and wanted to be able to pass a Closure as default value.
The only package I found with this feature is Illuminate/Support, which is too much.

## Installation

Using Composer, simply run:

```shell
composer require arcesilas/dot-array
```

## Usage

Before v2.0.0, DotArray could be built with an `array` or an instance of `ArrayAccess`.

As of v2.0.0, the constructor only accepts arrays, because `ArrayAccess` is not compatible with `array_*` functions, especially `array_replace_recursive` which is used for import.

If you have an `ArrayObject` or an `ArrayIterator` you want to use to create a new `DotArray`, you may use their `getArrayCopy()` method and pass the array to the constructor.

Or you can use `import` the object:

```php
$arrayObject = new ArrayObject(['foo' => 'bar']);
$dotArray = new DotArray();
$dotArray->import($arrayObject);
```

### Get

```php
$array = ['a' => ['b' => 'c']];
$dotArray = new DotArray($array);
$dotArray->get('a.b'); // c
```

You may specify a default value:

```php
$array = ['a' => ['b' => 'c']];
$dotArray = new DotArray($array);
$dotArray->get('b.a', 'zzz'); // zzz
```

Default value can be a Closure, which will be executed:

```php
$array = ['a' => ['b' => 'c']];
$dotArray = new DotArray($array);
$dotArray->get('b.a', function () {return 6 * 7}); // 42
```

### Set

Obviously:

```php
$array = ['a' => ['b' => 'c']];
$dotArray = new DotArray($array);
$dotArray->set('b.a', 'zzz');

$dotArray->get('b.a', 'zzz'); // zzz
```

### getArrayCopy

As of v2.0.0, this method replaces `export()`, for compatibility with `ArrayObject` and `ArrayIterator`.

This method returns the raw array stored in the `DotArray`:

```php
$array = ['a' => ['b' => 'c']];
$dotArray = new DotArray($array);
$dotArray->getArrayCopy();
/*
[
    'a' => [
        'b' => 'c'
    ]
]
*/
```

### Import

Import an array, an instance of `ArrayObejct` or `ArrayIterator` or a `DotArray`: keys will be incorporated with `array_replace_recursive`:

```
$input = [
    'a' => 'b',
    'c' => [
        'd' => 'e'
    ]
];

$dotArray = new DotArray($input);
$dotArray->import(['a' => 'c', 'f' => 'g']);
$dotArray->getArrayCopy();

// Returns:
$input = [
    'a' => 'c',
    'c' => [
        'd' => 'e'
    ],
    'f' => 'g'
];
```

## Cache

You don't need to be afraid of using `DotArray::has()` before using `DotArray::get()`: values are "cached" with heir dotted key.

Cache is reset after an import.

## ArrayAccess

DotArray implements ArrayAccess interface, so you can use it as an array:

```php
$dotArray->get('a.b.c');

// is equivalent to

$dotArray['a.b.c'];
```

The only drawback is you cannot specify a default value.
